# Angular Elements

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.9.

## Getting Started

1. Installed Style and Lint settings from Confulence
1. Installed Elements `ng add @angular/elements`

## Tasks

1. Creating a schematic to create and unwrap an Angular element
   > Since creating a custom element is pretty involved, we turned to schematics as the perfect solution for scaffolding out new elements, then “unwrapping” them once we are ready to convert the work to an Angular route. This solution reduces the seven steps listed above into a single command! Not only does this streamline the process, but it creates a standard, repeatable procedure that developers can follow. Now they can focus on building new Angular code rather than the intricacies of wiring up a new element.
   >
   > Resource #3
1. Routing

   We will need to host both Angular and AngularJS side by side. This will allow us to migrate routes as we go. Once the AngularJS route is fully converted to Angular Elements, we can create the route in Angular and the Angular container components. Unwrap the elements and away we go.

   ![Routing Diagram](./docs/angular-routing.jpg)

1. Promise to Obervable helpers with Zone injection

   > ...we have created a utility function that allows us to convert aPromise to an Observable while hooking back into NgZone all at the same time. This has been incredibly fruitful since a majority of the asynchronous work being done in AngularJS isPromisebased. The key thing to note about this is that hooking `intoNgZone.run()` is best used inside services and **not** in components.
   >
   > Resource #4

1. Need a detailed plan of how we want to go about breaking down VM into consumable chuncks to migrate

## Points of Interest

### Change Detection

This will be something we will have to be aware of.

### Element Selector Naming

Defining elements with the exact same name as the component will cause a double render of the component if it's being used as a child component for another element

**Do** define elements with a `-el` postfix.

### NgService usage

> In our experience, we have found great success creating “wrapper” services that grab references to the AngularJS providers and make them available via Angular’s dependency injection system. This allows us to create one class that will maintain the consumption of this legacy provider in one service, and when we are ready to migrate the provider’s logic, we only need to update the code in one place.
>
> Resource #4

### Routing

We will need to take care in how we slice up AngularJS into Elements as we cannot use the Angular router within Elements, they must all be 'single-page' components.

> ### A Word About Bundling
>
> In practice you will likely bootstrap several >independent elements as you move through the upgrade process.
>
> **One limitation of the Angular CLI is that it only supports a single bootstrap point per project.** This means we can’t output multiple element bundles from the same project. We recommend you avoid creating separate projects for each element since doing so will prevent you from tree shaking across all elements. As a result you would hypothetically get lots of duplicated third party dependencies bundled in with each element.
>
> **Instead we recommend you do use a single build process for all elements.** If you want to use the Angular CLI, you would have to customize the build process slightly through a custom builder. An alternative is to use Webpack directly since it gives you full control over the Webpack config. In both cases it comes down to creating a standard Webpack config with support for multiple inputs and multiple outputs.
>
> Resource #2

## Estimate (super sized ballpark)

Start simple, small pieces that focus on core - reusable component (tables, modals, filters, headers, forms).

10 days for design works

- module layout
- routing
- tools/tech (NGRX)

10-15 day per route (8 avg)
45 Sections (route groupings)

360 days of dev effort

**VS**

Full rebuild

- 180 days of dev effort (3 months with 3 devs)

### Setting Breakdown

Key requirments

- Releasable

## Breakdown of Settings

## Resources

1. [Using Angular Elements — Why and How?](https://blog.bitsrc.io/using-angular-elements-why-and-how-part-1-35f7fd4f0457)
1. [Upgrading AngularJS to Angular using Elements](https://blog.nrwl.io/upgrading-angularjs-to-angular-using-elements-f2960a98bc0e)
1. [How Capital One is Using Angular Elements to Upgrade from AngularJS](https://medium.com/capital-one-tech/capital-one-is-using-angular-elements-to-upgrade-from-angularjs-to-angular-42f38ef7f5fd)
1. [Upgrading with Angular Elements: Top Lessons Learned](https://medium.com/capital-one-tech/upgrading-with-angular-elements-eb3185e26382)

## Development server

Run `npm run start` for a dev server. Navigate to `http://localhost:8080/`. The app will automatically reload if you run another build.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory and concatenated into the `preview/` folder.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
