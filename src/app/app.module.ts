import { DoBootstrap, Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { FeatureModule } from './feature/feature.module';
import { FeatureComponent } from './feature/feature/feature.component';
import { HelloWorldComponent } from './hello-world/hello-world.component';
import { NgService } from './ng-resource.service';
import { TitleCardComponent } from './title-card/title-card.component';

@NgModule({
  declarations: [HelloWorldComponent, TitleCardComponent],
  imports: [BrowserModule, FeatureModule],
  entryComponents: [HelloWorldComponent, TitleCardComponent],
  providers: [NgService]
})
export class AppModule implements DoBootstrap {
  constructor(injector: Injector) {
    const helloWorldComponent = createCustomElement(HelloWorldComponent, { injector });
    customElements.define('roller-hello-world', helloWorldComponent);

    const titleCardComponent = createCustomElement(TitleCardComponent, { injector });
    customElements.define('roller-title-card', titleCardComponent);

    const featureComponent = createCustomElement(FeatureComponent, { injector });
    customElements.define('roller-feature', featureComponent);

    // const pageOneComponent = createCustomElement(PageOneComponent, { injector });
    // customElements.define('roller-page-one', pageOneComponent);
    // const pageTwoComponent = createCustomElement(PageTwoComponent, { injector });
    // customElements.define('roller-page-two', pageTwoComponent);
  }

  // eslint-disable-next-line @angular-eslint/no-empty-lifecycle-method, @typescript-eslint/no-empty-function
  ngDoBootstrap() {}
}
