import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FeatureComponent } from './feature/feature.component';
import { PageOneComponent } from './page-one/page-one.component';
import { PageTwoComponent } from './page-two/page-two.component';

@NgModule({
  declarations: [PageOneComponent, PageTwoComponent, FeatureComponent],
  imports: [CommonModule, ReactiveFormsModule],
  entryComponents: [PageOneComponent, PageTwoComponent, FeatureComponent],
  providers: []
})
export class FeatureModule {}
