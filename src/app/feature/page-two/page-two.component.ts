import { Component } from '@angular/core';
import { NgService } from 'src/app/ng-resource.service';

@Component({
  selector: 'roller-page-two',
  templateUrl: './page-two.component.html',
  styleUrls: ['./page-two.component.scss']
})
export class PageTwoComponent {
  loaded = false;
  data: any;

  private surveyService = this.ngService.getResource('surveyService') as any;

  constructor(private ngService: NgService) {
    this.surveyService?.getDashboard().then((res: any) => {
      console.log('res', res);
      this.loaded = true;
      this.data = res;
    });
  }
}
