import { Injectable } from '@angular/core';
import { NgService } from 'src/app/ng-resource.service';

@Injectable({
  providedIn: 'root'
})
export class PageTwoService {
  private surveyService = this.ngService.getResource('surveyService') as any;

  constructor(private ngService: NgService) {}

  getDashboard(): any {
    return this.surveyService.getDashboard();
  }
}
