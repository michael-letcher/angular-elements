import { TestBed } from '@angular/core/testing';

import { PageTwoService } from './page-two.service';

describe('PageTwoService', () => {
  let service: PageTwoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PageTwoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
