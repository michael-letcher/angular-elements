import { Component } from '@angular/core';

@Component({
  selector: 'roller-feature',
  templateUrl: './feature.component.html',
  styleUrls: ['./feature.component.scss']
})
export class FeatureComponent {}
