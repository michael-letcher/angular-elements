import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NgService } from 'src/app/ng-resource.service';

@Component({
  selector: 'roller-page-one',
  templateUrl: './page-one.component.html',
  styleUrls: ['./page-one.component.scss']
})
export class PageOneComponent {
  @Output() submitted = new EventEmitter();

  profileForm = this.fb.group({
    firstName: [''],
    lastName: [''],
    language: [''],
    address: this.fb.group({
      street: [''],
      city: [''],
      state: [''],
      zip: ['']
    })
  });

  private languageCodeConstant = this.ngService.getResource<string>('languageCode');

  constructor(private fb: FormBuilder, private ngService: NgService) {
    this.profileForm.get('language')?.setValue(this.languageCodeConstant);
  }

  onSubmit(): void {
    this.submitted.emit(this.profileForm.value);
  }
}
