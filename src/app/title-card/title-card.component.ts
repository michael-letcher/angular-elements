import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'roller-title-card',
  templateUrl: './title-card.component.html',
  styleUrls: ['./title-card.component.scss']
})
export class TitleCardComponent {
  @Input() title = '';
  @Input() rname = '';
  @Input() occupation = '';
  @Input() location = '';
  @Input() first = '';

  @Output() learnMore = new EventEmitter();

  onLearnMore(): void {
    this.learnMore.emit('Learn more has been clicked');
  }
}
